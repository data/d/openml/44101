# OpenML dataset: white_wine_quality

https://www.openml.org/d/44101

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Source**

Paulo Cortez, University of Minho, Guimaraes, Portugal, http://www3.dsi.uminho.pt/pcortez
A. Cerdeira, F. Almeida, T. Matos and J. Reis, Viticulture Commission of the Vinho Verde Region(CVRVV), Porto, Portugal
@2009

This dataset was originally obtained from the [UCI repository](https://archive.ics.uci.edu/ml/datasets/wine+quality).


**Data description**

This dataset is related to the white variant of the "Vinho Verde" wine.
Almost 20% of the original dataset were duplicates. As all features are continuous, the chance
that those rows are different wines is very slim. Because of that, those observations
were removed in order to be better judge the generalization capacity of the algorithms

These datasets can be viewed as classification or regression tasks. The classes
are ordered and not balanced (e.g. there are many more normal wines than
excellent or poor ones). Outlier detection algorithms could be used to detect
the few excellent or poor wines. Also, we are not sure if all input variables
are relevant. So it could be interesting to test feature selection methods.

**Feature description**

Input variables (based on physicochemical tests):

 - fixed acidity
 - volatile acidity
 - citric acid
 - residual sugar
 - chlorides
 - free sulfur dioxide
 - total sulfur dioxide
 - density
 - pH
 - sulphates
 - alcohol

Output variable (based on sensory data):

 - quality (score between 0 and 10)

**Citation Request**

Please include this citation if you plan to use this database:

P. Cortez, A. Cerdeira, F. Almeida, T. Matos and J. Reis.
Modeling wine preferences by data mining from physicochemical properties. In Decision Support Systems, Elsevier, 47(4):547-553, 2009.

@article{cortez2009modeling,
  title={Modeling wine preferences by data mining from physicochemical properties},
  author={Cortez, Paulo and Cerdeira, Ant{\'o}nio and Almeida, Fernando and Matos, Telmo and Reis, Jos{\'e}},
  journal={Decision support systems},
  volume={47},
  number={4},
  pages={547--553},
  year={2009},
  publisher={Elsevier}
}

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44101) of an [OpenML dataset](https://www.openml.org/d/44101). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44101/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44101/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44101/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

